import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  url:string = 'https://api.darksky.net/forecast/ea76e78f539ef7dae1879fd1a45d3628/';
  lat:number;
  lng:number;

  /*Information to show */
  information:any;
  timezone:string = '';
  temperature:string = ''
  humidity:string = '';
  rain:string = '';
  summary:string = '';

  constructor(public navCtrl: NavController,
              public http: HttpClient,
              public geolocation: Geolocation,
              public toastCtrl: ToastController) {

                this.load();
  } 

  load(){
    this.getLocation().then(res =>{
      this.getWeather();
    }).catch(err =>{
      console.error('Location no fount to get weather information');
      let toast = this.toastCtrl.create({
        message: 'Location no fount to get weather information, get default Cali,CO',
        duration: 3000
      });
      toast.present();
      this.lat = 3.3950619;
      this.lng = -76.5957046;
      this.getWeather();
    });   

  }

  getLocation(){
      return new Promise<any>((resolve, reject)=>{
        this.geolocation.getCurrentPosition().then((resp) => {
          this.lat = resp.coords.latitude;
          this.lng = resp.coords.longitude;
          resolve(resp);
         }).catch((error) => {
           console.log('Error getting location', error);
           reject(error);
         });
      });
  }

  getWeather(){
    let url_api = this.url + this.lat + ',' + this.lng;
    this.http.get(url_api).subscribe(data =>{
        //console.log('getWeather  ' + JSON.stringify(data));        
        this.timezone = data['timezone'];        
        this.temperature = Math.round(((data['currently'].temperature -32) * 5 / 9)).toString();
        this.humidity = data['currently'].humidity;
        this.rain = data['currently'].precipProbability;
        this.summary = data['currently'].summary;
    }, err =>{
        console.error('http error ' + err);
        let toast = this.toastCtrl.create({
          message: 'cannot obtain weather information',
          duration: 3000
        });
        toast.present();

        this.timezone = '';
        this.temperature = '';
        this.humidity = '';
        this.rain = '';
        this.summary = '';
    });
  }

}
